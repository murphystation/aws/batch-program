let axios = require('axios');

/**
 * Environment Variables
 * slack_url
 * scrip
 * refId
 */

async function notifySlack(scrip, refId){

    let dt = new Date()
    // Send some random data
    let data = JSON.stringify({
        text: `${dt.toISOString()} (${dt.getTime()}) :: Working with ${scrip} and the ${refId}`
    });

    let config = {
        method: 'post',
        url: process.env.slack_url,
        headers: {
            'Content-Type': 'application/json'
        },
        data: data
    };

    await axios(config)
    return true
}

function main(){
    //
    let scrip = process.env.scrip || "RANDOM"
    let refId = process.env.refId || "RANDOM"

    notifySlack(scrip, refId)
}


if(require.main == module){
    main()
}